﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RingoRole : MonoBehaviour {


	public float root;

	private string Status;

	bool flg_A;
	bool flg_W;
	bool flg_D;
	bool flg_S;

	public static bool Next_flg_A;
	public static bool Next_flg_W;
	public static bool Next_flg_D;
	public static bool Next_flg_S;

	private Rigidbody rb;

	[SerializeField]
	private Text Bad;

	public static bool Gool_Flg=false;

    [SerializeField]
    private AudioSource SE;

    [SerializeField]
    private AudioClip[] SE_Clip;





	// Use this for initialization
	void Start () {

		Next_flg_A = true;
		Next_flg_W = false;
		Next_flg_D = false;
		Next_flg_S = false;

		flg_A = false;
		flg_W = false;
		flg_D = false;
		flg_S = false;

		rb = GetComponent<Rigidbody> ();

		Bad.enabled = false;

		
	}
	
	// Update is called once per frame
	void Update () 
	{

		if (GameManager.GoGame&&!Gool_Flg) {
            
			rb.useGravity = true;

			if (Input.GetKeyDown ("a") && !flg_A) {
				if (Next_flg_A) {
					flg_A = true;
					Status = "A";
                    SE.PlayOneShot(SE_Clip[0]);

				}

				if (!flg_A) {
					Debug.Log ("Eroor");
					Bad.text = "なんでやねん！それはAだ！タイム＋２";
					GameManager.Nowtime += 2;
					Bad.enabled = true;
					Invoke ("BadDel", 1.0f);
                    SE.PlayOneShot(SE_Clip[1]);
                    SE.PlayOneShot(SE_Clip[3]);

				}
			}

			if (Input.GetKeyDown ("w")) {
				if (Next_flg_W) {
					flg_W = true;
					Status = "W";
                    SE.PlayOneShot(SE_Clip[0]);
				}

				if (!flg_W) {
					Debug.Log ("Eroor");
                    Bad.text = "なんでやねん！それはWだ！タイム＋２";
					GameManager.Nowtime += 2;
					Bad.enabled = true;
					Invoke ("BadDel", 1.0f);
                    SE.PlayOneShot(SE_Clip[1]);
                    SE.PlayOneShot(SE_Clip[3]);
				}

			}

			if (Input.GetKeyDown ("d")) {
				if (Next_flg_D) {
					flg_D = true;
					Status = "D";
                    SE.PlayOneShot(SE_Clip[0]);
				}

				if (!flg_D) {
					Debug.Log ("Eroor");
                    Bad.text = "なんでやねん！それはDだ！タイム＋２";
					GameManager.Nowtime += 2;
					Bad.enabled = true;
					Invoke ("BadDel", 1.0f);
                    SE.PlayOneShot(SE_Clip[1]);
                    SE.PlayOneShot(SE_Clip[3]);
				}
			}

			if (Input.GetKeyDown ("s")) {
				if (Next_flg_S) {
					flg_S = true;
					Status = "S";
                    SE.PlayOneShot(SE_Clip[0]);
				}

				if (!flg_S) {
					Debug.Log ("Eroor");
                    Bad.text = "なんでやねん！それはSだ！！タイム＋２";
					GameManager.Nowtime += 2;
					Bad.enabled = true;
					Invoke ("BadDel", 1.0f);
                    SE.PlayOneShot(SE_Clip[1]);
                    SE.PlayOneShot(SE_Clip[3]);
				}
			}
		}

	}

	void BadDel()
	{
		Bad.enabled = false;

	}

	void FixedUpdate()
	{
		switch (Status) 
		{
		case "A":
			if (flg_A) {
				//等速アニメーション
				this.transform.position += new Vector3 (0.5f, 0, 0);
				this.transform.rotation = Quaternion.Euler (0, 0, root);
				root -= 30;
				Status = null;
				flg_A = false;
				Next_flg_W = true;
				Next_flg_A = false;
			}
			break;
		case "W":
			if (flg_W) {
				//等速アニメーション
				this.transform.position += new Vector3 (0.5f, 0, 0);
				this.transform.rotation = Quaternion.Euler (0, 0, root);
				root -= 30;
				Status = null;
				flg_W = false;
				Next_flg_W = false;
				Next_flg_D = true;
			}
			break;
		case "D":
			if (flg_D) {
				//等速アニメーション
				this.transform.position += new Vector3 (0.5f, 0, 0);
				this.transform.rotation = Quaternion.Euler (0, 0, root);
				root -= 30;
				Status = null;
				flg_D = false;
				Next_flg_D = false;
				Next_flg_S = true;

			}
			break;

		case "S":
			if (flg_S) 
			{
				//等速アニメーション
				this.transform.position += new Vector3 (0.5f, 0, 0);
				this.transform.rotation = Quaternion.Euler (0, 0, root);
				root -= 30;
				Status = null;
				flg_S = false;
				Next_flg_S = false;
				Next_flg_A = true;

			}
			break;

		}
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.CompareTag("Goal")) 
		{
			Debug.Log ("hoge");
			Gool_Flg = true;
            SE.PlayOneShot(SE_Clip[2]);
		}

	}

}
