﻿/// <summary>
/// 作成者:中川良平
/// 日付　:1月27日
/// シート名:敵クラス
/// </summary>
using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	private Rigidbody ri;
	public float pw_y_max;
	public float pw_y_min;
	public float pw_x_max;
	public float pw_x_min;
	public float pw_z_max;
	public float pw_z_min;

	// Use this for initialization
	void Start () {
		//物理演算取得
		ri = gameObject.GetComponent<Rigidbody>();
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	//プレイヤーの攻撃が当たった時のイベント
	void OnTriggerEnter (Collider collision ) {


		if (collision.gameObject.tag == "hit") 
		{
			float up_pw;
			float x_pw;
			float z_pw;

			PlayerCtl.Attack_flg = false;
			up_pw = Random.Range (pw_y_min,pw_y_max);
			x_pw = Random.Range (pw_x_min,pw_x_max);
			z_pw = Random.Range (pw_z_min,pw_z_max);
			ri.AddForce(Vector3.up*up_pw, ForceMode.Impulse);
			ri.AddForce(Vector3.right*x_pw, ForceMode.Impulse);
			ri.AddForce(Vector3.forward*z_pw, ForceMode.Impulse);
			PlayerCtl.Right_Move = false;
			PlayerCtl.Left_Move = false;


		}

		if (collision.gameObject.tag == "out") 
		{
			Destroy (this.gameObject);
		}
	}
}
