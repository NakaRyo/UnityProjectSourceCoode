﻿/// <summary>
/// 作成者:中川良平
/// 日付　:1月27日
/// シート名:プレイヤースクリプト
/// </summary>
using UnityEngine;
using System.Collections;

public class PlayerCtl : MonoBehaviour {

	public float speed;
	public float jamp_pw = 5.0f;
	public static bool Right_Move=false;
	public static bool Left_Move=false;
	public static bool Attack_flg=false;
	bool Jump_flg=false;

	//当たり判定よう
	private Rigidbody ri;
	public float pw_y_min;
	public float pw_y_max;
	public float pw_x_min;
	public float pw_x_max;
	public float pw_z_min;
	public float pw_z_max;

	[SerializeField]
	GameObject hitcol;



	[SerializeField]
	GameManager flg;

	private Animator anim;

	[SerializeField]
	AudioSource Se;

	[SerializeField]
	AudioClip[] SeClip;



	// Use this for initialization
	void Start () 
	{

		flg.Goal_flag = false;
		//物理演算取得
		ri = gameObject.GetComponent<Rigidbody>();
		Right_Move=false;
		Left_Move=false;
		anim = GetComponent<Animator>();
		
	
	}
	
	// Update is called once per frame
	void Update () {
		
		if(flg.Start_flag)
		{
			
			Move ();
			anim.SetBool ("Dash", true);


		}

	}
		

	void Move()
	{
		
		transform.position += transform.forward * speed;
		//右旋回
		if (Input.GetMouseButtonDown(0) && Right_Move)
		{
			transform.Rotate (0,90, 0);
			Debug.Log ("Right="+Right_Move);
			Se.PlayOneShot(SeClip[2]);
		}
		//左旋回
		if (Input.GetMouseButtonDown (0) && Left_Move)
		{
				//transform.position += transform.forward* speed;
				transform.Rotate (0,-90, 0);
			Debug.Log ("Left="+Left_Move);
			Se.PlayOneShot(SeClip[2]);
		}
		//ジャンピング
		if (Input.GetMouseButtonDown (0) && Jump_flg) 
		{
			Jump_flg = false;
			anim.SetTrigger ("Jump");
			Se.PlayOneShot(SeClip[3]);
			this.GetComponent<Rigidbody> ().velocity = new Vector3 (this.GetComponent<Rigidbody> ().velocity.x,jamp_pw, 0);
			if (Right_Move) {
				transform.Rotate (0, -90, 0);
			} 
			else if(Left_Move) 
			{
				transform.Rotate (0, 90, 0);
			}
		}

		//攻撃処理
		if (Input.GetMouseButtonDown (0) && Attack_flg) 
		{
			Se.PlayOneShot(SeClip[0]);
			anim.SetTrigger ("Attack");
			anim.SetBool ("Dash", false);
			hitcol.SetActive (true);
			Invoke ("hitdel", 0.3f);
			if (Right_Move) 
			{
				transform.Rotate (0, -90, 0);
			} 
			else if(Left_Move) 
			{
				transform.Rotate (0, 90, 0);
			}
			Left_Move = false;
			Right_Move = false;
		}

		//transform.Rotate (0,90, 0);
	}

	public void hitdel()
	{
		hitcol.SetActive (false);
	}

	//物理演算無視の当たり判定での動作切り替え処理
	void OnTriggerEnter (Collider collision ) {


		if (collision.gameObject.tag == "right") 
		{
			Right_Move = true;
			Left_Move = false;
			Attack_flg = false;
			Debug.Log ("Right="+Right_Move);
		}

		if (collision.gameObject.tag == "left") 
		{
			Left_Move = true;
			Right_Move = false;
			Attack_flg = false;
			Debug.Log ("Left="+Left_Move);

		}

		if (collision.gameObject.tag == "goal")
		{
			flg.Goal_flag = true;
			speed = 0.0f;
			anim.SetBool ("Yorokobi", true);
			Se.PlayOneShot (SeClip [4]);
		}


		if (collision.gameObject.tag == "jump")
		{
			
			Jump_flg = true;

		}

		if (collision.gameObject.tag == "enemy_point")
		{

			Attack_flg = true;
			Debug.Log ("Atk="+Attack_flg);


		}

		if (collision.gameObject.tag == "out")
		{

			flg.GameOver_flag = true;
			UserStatus.Retry_num++;
			this.gameObject.SetActive (false);

		}
			
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "enemy")
		{

			Se.PlayOneShot(SeClip[1]);


			//flg.GameOver_flag = true;
			flg.Camera_flag=true;
			float y_pw;
			float x_pw;
			float z_pw;
			Attack_flg = false;
			//乱数を代入
			y_pw = Random.Range (pw_y_min,pw_y_max);
			x_pw = Random.Range (pw_x_min,pw_x_max);
			z_pw = Random.Range (pw_z_min,pw_z_max);

			//力を与える
			if (Left_Move) {
				ri.AddForce (Vector3.up * y_pw, ForceMode.Impulse);
				ri.AddForce (Vector3.right * x_pw, ForceMode.Impulse);
				ri.AddForce (Vector3.back * z_pw, ForceMode.Impulse);
				Right_Move = true;
				Left_Move = true;
			} else if (Right_Move) {
				ri.AddForce (Vector3.up * y_pw, ForceMode.Impulse);
				ri.AddForce (Vector3.left * x_pw, ForceMode.Impulse);
				ri.AddForce (Vector3.forward * z_pw, ForceMode.Impulse);
				Right_Move = true;
				Left_Move = true;
			} 
			else 
			{
				ri.AddForce (Vector3.up * y_pw, ForceMode.Impulse);
				ri.AddForce (Vector3.right * x_pw, ForceMode.Impulse);
				ri.AddForce (Vector3.back * z_pw, ForceMode.Impulse);
				Right_Move = true;
				Left_Move = true;
				
			}


		}
	}
		
}