﻿/// <summary>
/// 作成者:中川良平
/// 日付　:1月27日
/// シート名:シーン制御スクリプト
/// </summary>
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneMove : MonoBehaviour {


	public  GameObject RetryButton;

	void Start()
	{
		
	}
	/// <summary>
	/// メニューからステージへ行く時
	/// </summary>
	public void GoStage()
	{
		//選択中ステージを習得
		UserStatus.StageNow = PlayerPrefs.GetString ("StageNameNow",UserStatus.StageNow);

		//遷移分け
		switch (UserStatus.StageNow) 
		{
		case "Stage_0":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);
				
			}
			else 
			{
				SceneManager.LoadScene ("Stage_1-1", LoadSceneMode.Single);
			}
	
			break;

		case "Stage_1":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				SceneManager.LoadScene ("Stage_1-2", LoadSceneMode.Single);
			}
			break;

		case "Stage_2":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				SceneManager.LoadScene ("Stage_1-3", LoadSceneMode.Single);
			}
			break;


		case "Stage_3":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				SceneManager.LoadScene ("Stage_2-1", LoadSceneMode.Single);
				//SceneManager.LoadScene ("Comming",LoadSceneMode.Single);
			}
			break;

		case "Stage_4":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				//SceneManager.LoadScene ("Stage_2-2", LoadSceneMode.Single);
				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);
			}
			break;

		case "Stage_5":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				//SceneManager.LoadScene ("Stage_2-3", LoadSceneMode.Single);
				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);
			}
			break;

		case "Stage_6":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				//SceneManager.LoadScene ("Stage_3-1", LoadSceneMode.Single);
				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);
			}
			break;

		case "Stage_7":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				//SceneManager.LoadScene ("Stage_3-2", LoadSceneMode.Single);
				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);
			}
			break;

		case "Stage_8":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				//SceneManager.LoadScene ("Stage_3-3", LoadSceneMode.Single);
				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);
			}
			break;

		case "Stage_9":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				//SceneManager.LoadScene ("Stage_4-1", LoadSceneMode.Single);
				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);
			}
			break;

		case "Stage_10":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				//SceneManager.LoadScene ("Stage_4-2", LoadSceneMode.Single);
				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);
			}
			break;

		case "Stage_11":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				//SceneManager.LoadScene ("Stage_4-3", LoadSceneMode.Single);
				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);
			}
			break;

		case "Stage_12":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				//SceneManager.LoadScene ("Stage_5-1", LoadSceneMode.Single);
				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);
			}
			break;

		case "Stage_13":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				//SceneManager.LoadScene ("Stage_5-2", LoadSceneMode.Single);
				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);
			}
			break;

		case "Stage_14":
			if (UserStatus.Hard) 
			{

				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);

			}
			else 
			{
				//SceneManager.LoadScene ("Stage_5-3", LoadSceneMode.Single);
				SceneManager.LoadScene ("Comming",LoadSceneMode.Single);
			}
			break;
		}
	}
	/// <summary>
	/// メニューへ遷移
	/// </summary>
	public void GoMenu()
	{
		//Application.LoadLevel("Menu");
		SceneManager.LoadScene ("Menu",LoadSceneMode.Single);
		UserStatus.GameBack = true;
		UserStatus.Retry_num = 0;
	}

	/// <summary>
	/// クリア画面へ遷移と同時に次のステージが未解放だったら解放
	/// </summary>
	public void GoClear()
	{

		SceneManager.LoadScene ("Resurut",LoadSceneMode.Single);
		UserStatus.Retry_num = 0;
		UserStatus.StageUnlock = PlayerPrefs.GetInt ("StageUnlock", UserStatus.StageUnlock);

		//ステージで順次解放チェック
		switch (UserStatus.StageNow) 
		{
		case "Stage_0":
			//次のステージになるので値が+1される
			if (UserStatus.StageUnlock < 1) 
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 1;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_1":
			if (UserStatus.StageUnlock < 2) 
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 2;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_2":
			if (UserStatus.StageUnlock < 3)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 3;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;


		case "Stage_3":
			if (UserStatus.StageUnlock < 4)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 4;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_4":
			if (UserStatus.StageUnlock < 5)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 5;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;
		case "Stage_5":
			if (UserStatus.StageUnlock < 6)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 6;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;
		case "Stage_6":
			if (UserStatus.StageUnlock < 7)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 7;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_7":
			if (UserStatus.StageUnlock < 8)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 8;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_8":
			if (UserStatus.StageUnlock < 9)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 9;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_9":
			if (UserStatus.StageUnlock < 10)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 10;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_10":
			if (UserStatus.StageUnlock < 11)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 11;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_11":
			if (UserStatus.StageUnlock < 12)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 12;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_12":
			if (UserStatus.StageUnlock < 13)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 13;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_13":
			if (UserStatus.StageUnlock < 14)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 14;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_14":
			if (UserStatus.StageUnlock < 15)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 15;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		}

	}

	/// <summary>
	/// ムービーでをさいせいしたら解放判定
	/// </summary>
	public void Move_clear()
	{
		GoMenu ();
		UserStatus.StageUnlock = PlayerPrefs.GetInt ("StageUnlock", UserStatus.StageUnlock);
		//ステージで順次解放チェック
		switch (UserStatus.StageNow) 
		{
		case "Stage_0":
			//次のステージになるので値が+1される
			if (UserStatus.StageUnlock < 1) 
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 1;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_1":
			if (UserStatus.StageUnlock < 2) 
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 2;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_2":
			if (UserStatus.StageUnlock < 3)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 3;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;


		case "Stage_3":
			if (UserStatus.StageUnlock < 4)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 4;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_4":
			if (UserStatus.StageUnlock < 5)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 5;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;
		case "Stage_5":
			if (UserStatus.StageUnlock < 6)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 6;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;
		case "Stage_6":
			if (UserStatus.StageUnlock < 7)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 7;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_7":
			if (UserStatus.StageUnlock < 8)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 8;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_8":
			if (UserStatus.StageUnlock < 9)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 9;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_9":
			if (UserStatus.StageUnlock < 10)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 10;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_10":
			if (UserStatus.StageUnlock < 11)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 11;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_11":
			if (UserStatus.StageUnlock < 12)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 12;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_12":
			if (UserStatus.StageUnlock < 13)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 13;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_13":
			if (UserStatus.StageUnlock < 14)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 14;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;

		case "Stage_14":
			if (UserStatus.StageUnlock < 15)
			{
				//アンロックするステージ数
				UserStatus.StageUnlock = 15;
				PlayerPrefs.SetInt ("StageUnlock", UserStatus.StageUnlock);
			}
			break;
		}
	}
		
	/// <summary>
	/// シーンごとのムービー解放ボタン出現判定
	/// </summary>
	public void Move_clearbutton()
	{

		UserStatus.StageUnlock = PlayerPrefs.GetInt ("StageUnlock", UserStatus.StageUnlock);
		//ステージで順次解放チェック
		switch (UserStatus.StageNow) {
		case "Stage_0":
			//次のステージになるので値が+1される
			if (UserStatus.StageUnlock < 1) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;

		case "Stage_1":
			if (UserStatus.StageUnlock < 2) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;

		case "Stage_2":
			if (UserStatus.StageUnlock < 3) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;


		case "Stage_3":
			if (UserStatus.StageUnlock < 4) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;

		case "Stage_4":
			if (UserStatus.StageUnlock < 5) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;
		case "Stage_5":
			if (UserStatus.StageUnlock < 6) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;
		case "Stage_6":
			if (UserStatus.StageUnlock < 7) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;

		case "Stage_7":
			if (UserStatus.StageUnlock < 8) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;

		case "Stage_8":
			if (UserStatus.StageUnlock < 9) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;

		case "Stage_9":
			if (UserStatus.StageUnlock < 10) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;

		case "Stage_10":
			if (UserStatus.StageUnlock < 11) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;

		case "Stage_11":
			if (UserStatus.StageUnlock < 12) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;

		case "Stage_12":
			if (UserStatus.StageUnlock < 13) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;

		case "Stage_14":
			if (UserStatus.StageUnlock < 15) {
				//次ステージアンロック解放ボタン表示
				RetryButton.SetActive (true);
			}
			break;

		}

	}

}
